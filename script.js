function filterBy(arr, type) {
    let filteredArray = [];

    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] !== type) {
            filteredArray.push(arr[i]);
        }
    }

    return filteredArray;
}

let arr = ['hello', 'world', 23, '23', null];
let filteredArray = filterBy(arr, 'string');

console.log(filteredArray);

